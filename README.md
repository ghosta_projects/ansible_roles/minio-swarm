minio-swarm
=========

This role will install s3 minio under docker swarm on your server.

Dependencies
------------

Docker role will be installed and run before the minio-swarm role

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - mimio-swarm
